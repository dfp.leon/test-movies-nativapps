import { defineStore } from 'pinia'

export const cartStore = defineStore('cart', {
  state: () => ({
    buying: [],
    renting: []
  }),
  getters: {
    quantity: (state) => state.buying.length + state.renting.length
  },
  actions: {
    addToCart(payload) {
      this[payload.type].push({ movie: payload.movie, qty: payload.qty, id: payload.id, date: payload.date })
    },
    deleteFromCart(payload) {
      console.log('called', payload)
      this[payload.type].splice(payload.index, 1)
    }
  },
  persist: true
})
